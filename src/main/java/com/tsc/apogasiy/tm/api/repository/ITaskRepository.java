package main.java.com.tsc.apogasiy.tm.api.repository;

import main.java.com.tsc.apogasiy.tm.enumerated.Status;
import main.java.com.tsc.apogasiy.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAllTaskByProjectId(String projectId);

    Task findByName(String name);

    Task removeByName(String name);

    boolean existsById(String id);

    boolean existsByIndex(Integer index);

    boolean existsByName(String name);

    Task startById(String id);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusByName(String name, Status status);

    Task bindTaskToProjectById(String projectId, String taskId);

    Task unbindTaskById(String taskId);

    void removeAllTaskByProjectId(String projectId);


}
