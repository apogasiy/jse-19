package com.tsc.apogasiy.tm.api.service;

import main.java.com.tsc.apogasiy.tm.enumerated.Role;
import main.java.com.tsc.apogasiy.tm.model.User;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    void add(User user);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User setRole(String userId, Role role);

    void remove(User user);

    void clear();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeById(String id);

    User removeByLogin(String login);

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    User updateById(String id, String lastName, String firstName, String middleName, String email);

    User updateByLogin(String login, String lastName, String firstName, String middleName, String email);

}
