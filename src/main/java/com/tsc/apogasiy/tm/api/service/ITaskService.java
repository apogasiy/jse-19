package main.java.com.tsc.apogasiy.tm.api.service;

import com.tsc.apogasiy.tm.api.service.IService;
import main.java.com.tsc.apogasiy.tm.enumerated.Status;
import main.java.com.tsc.apogasiy.tm.model.Task;

public interface ITaskService extends IService<Task> {

    void create(String name);

    void create(String name, String description);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    Task updateById(final String id, final String name, final String description);

    Task updateByIndex(final Integer index, final String name, final String description);

    boolean existsByIndex(int index);

    boolean existsByName(String name);

    Task startById(String id);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusByName(String name, Status status);

}
