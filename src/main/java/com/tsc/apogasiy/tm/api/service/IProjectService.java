package main.java.com.tsc.apogasiy.tm.api.service;

import com.tsc.apogasiy.tm.api.service.IService;
import main.java.com.tsc.apogasiy.tm.enumerated.Status;
import main.java.com.tsc.apogasiy.tm.model.Project;

public interface IProjectService extends IService<Project> {

    void create(String name);

    void create(String name, String description);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project updateById(final String id, final String name, final String description);

    Project updateByIndex(final Integer index, final String name, final String description);

    boolean existsByIndex(Integer index);

    boolean existsByName(String name);

    Project startById(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    Project changeStatusByName(String name, Status status);

}
