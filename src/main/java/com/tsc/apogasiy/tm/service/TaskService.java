package main.java.com.tsc.apogasiy.tm.service;

import main.java.com.tsc.apogasiy.tm.api.repository.ITaskRepository;
import main.java.com.tsc.apogasiy.tm.api.service.ITaskService;
import main.java.com.tsc.apogasiy.tm.enumerated.Status;
import main.java.com.tsc.apogasiy.tm.exception.empty.*;
import main.java.com.tsc.apogasiy.tm.exception.entity.TaskNotFoundException;
import main.java.com.tsc.apogasiy.tm.exception.system.IndexIncorrectException;
import main.java.com.tsc.apogasiy.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        if (description == null || description.isEmpty())
            throw new EmptyDescriptionException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public Task findByName(String name) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return taskRepository.findByName(name);
    }

    @Override
    public Task removeByName(String name) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return taskRepository.removeByName(name);
    }

    @Override
    public Task updateById(String id, String name, String description) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        final Task task = taskRepository.findById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(Integer index, String name, String description) {
        if (index == null)
            throw new EmptyIndexException();
        if (index < 0)
            throw new IndexIncorrectException();
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        final Task task = taskRepository.findByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public boolean existsById(String id) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        return taskRepository.existsById(id);
    }

    @Override
    public boolean existsByIndex(int index) {
        return taskRepository.existsByIndex(index);
    }

    @Override
    public boolean existsByName(String name) {
        return taskRepository.existsByName(name);
    }

    @Override
    public Task startById(String id) {
        if (id == null || id.isEmpty())
            throw new EmptyNameException();
        return taskRepository.startById(id);
    }

    @Override
    public Task startByIndex(Integer index) {
        if (index == null)
            throw new EmptyIndexException();
        if (index < 0)
            throw new IndexIncorrectException();
        return taskRepository.startByIndex(index);
    }

    @Override
    public Task startByName(String name) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return taskRepository.startByName(name);
    }

    @Override
    public Task finishById(String id) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        return taskRepository.finishById(id);
    }

    @Override
    public Task finishByIndex(Integer index) {
        if (index == null)
            throw new EmptyIndexException();
        if (index < 0)
            throw new IndexIncorrectException();
        return taskRepository.finishByIndex(index);
    }

    @Override
    public Task finishByName(String name) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return taskRepository.finishByName(name);
    }

    @Override
    public Task changeStatusById(String id, Status status) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        if (status == null)
            throw new EmptyStatusException();
        return taskRepository.changeStatusById(id, status);
    }

    @Override
    public Task changeStatusByIndex(Integer index, Status status) {
        if (index == null)
            throw new EmptyIndexException();
        if (index < 0)
            throw new IndexIncorrectException();
        if (status == null)
            throw new EmptyStatusException();
        return taskRepository.changeStatusByIndex(index, status);
    }

    @Override
    public Task changeStatusByName(String name, Status status) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        if (status == null)
            throw new EmptyStatusException();
        return taskRepository.changeStatusByName(name, status);
    }

}
