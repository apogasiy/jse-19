package main.java.com.tsc.apogasiy.tm.service;

import main.java.com.tsc.apogasiy.tm.api.repository.IProjectRepository;
import main.java.com.tsc.apogasiy.tm.api.service.IProjectService;
import main.java.com.tsc.apogasiy.tm.enumerated.Status;
import main.java.com.tsc.apogasiy.tm.exception.empty.*;
import main.java.com.tsc.apogasiy.tm.exception.system.IndexIncorrectException;
import main.java.com.tsc.apogasiy.tm.model.Project;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        if (description == null || description.isEmpty())
            throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public Project findByName(String name) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return projectRepository.findByName(name);
    }

    @Override
    public Project updateById(String id, String name, String description) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        final Project project = projectRepository.findById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(Integer index, String name, String description) {
        if (index == null)
            throw new EmptyIndexException();
        if (index < 0)
            throw new IndexIncorrectException();
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        final Project project = projectRepository.findByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public boolean existsByName(String name) {
        return projectRepository.existsByName(name);
    }

    @Override
    public Project startById(String id) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        return projectRepository.startById(id);
    }

    @Override
    public Project startByIndex(Integer index) {
        if (index == null)
            throw new EmptyIndexException();
        if (index < 0)
            throw new IndexIncorrectException();
        return projectRepository.startByIndex(index);
    }

    @Override
    public Project startByName(String name) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return projectRepository.startByName(name);
    }

    @Override
    public Project finishById(String id) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        return projectRepository.finishById(id);
    }

    @Override
    public Project finishByIndex(Integer index) {
        if (index == null)
            throw new EmptyIndexException();
        if (index < 0)
            throw new IndexIncorrectException();
        return projectRepository.finishByIndex(index);
    }

    @Override
    public Project finishByName(String name) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return projectRepository.finishByName(name);
    }

    @Override
    public Project changeStatusById(String id, Status status) {
        if (id == null || id.isEmpty())
            throw new EmptyIdException();
        if (status == null)
            throw new EmptyStatusException();
        return projectRepository.changeStatusById(id, status);
    }

    @Override
    public Project changeStatusByIndex(Integer index, Status status) {
        if (index == null)
            throw new EmptyIndexException();
        if (index < 0)
            throw new IndexIncorrectException();
        if (status == null)
            throw new EmptyStatusException();
        return projectRepository.changeStatusByIndex(index, status);
    }

    @Override
    public Project changeStatusByName(String name, Status status) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        if (status == null)
            throw new EmptyStatusException();
        return projectRepository.changeStatusByName(name, status);
    }
}
