package main.java.com.tsc.apogasiy.tm.command.project;

import main.java.com.tsc.apogasiy.tm.command.AbstractProjectCommand;
import main.java.com.tsc.apogasiy.tm.exception.entity.ProjectNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.Project;
import main.java.com.tsc.apogasiy.tm.util.TerminalUtil;

public class ProjectShowByNameCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return "project-show-by-name";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show project by name";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findByName(name);
        if (project == null)
            throw new ProjectNotFoundException();
        showProject(project);
    }

}
