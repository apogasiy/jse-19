package main.java.com.tsc.apogasiy.tm.command.task;

import main.java.com.tsc.apogasiy.tm.command.AbstractTaskCommand;
import main.java.com.tsc.apogasiy.tm.exception.entity.TaskNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.Task;
import main.java.com.tsc.apogasiy.tm.util.TerminalUtil;

public class TaskStartByNameCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return "task-start-by-name";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Start task by name";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findByName(name);
        if (task == null)
            throw new TaskNotFoundException();
        serviceLocator.getTaskService().startByName(name);
    }

}
