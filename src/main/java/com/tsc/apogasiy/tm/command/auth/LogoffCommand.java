package main.java.com.tsc.apogasiy.tm.command.auth;

import main.java.com.tsc.apogasiy.tm.command.user.AbstractAuthCommand;

public class LogoffCommand extends AbstractAuthCommand {

    @Override
    public String getCommand() {
        return "logoff";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "User logoff from system";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().logout();
    }

}
