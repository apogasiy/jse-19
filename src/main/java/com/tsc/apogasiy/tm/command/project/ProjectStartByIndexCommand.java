package main.java.com.tsc.apogasiy.tm.command.project;

import main.java.com.tsc.apogasiy.tm.command.AbstractProjectCommand;
import main.java.com.tsc.apogasiy.tm.exception.entity.ProjectNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.Project;
import main.java.com.tsc.apogasiy.tm.util.TerminalUtil;

public class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return "project-start-by-index";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Start project by index";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber();
        final Project project = serviceLocator.getProjectService().findByIndex(index);
        if (project == null)
            throw new ProjectNotFoundException();
        serviceLocator.getProjectService().startByIndex(index);
    }

}
