package main.java.com.tsc.apogasiy.tm.command.system;

import main.java.com.tsc.apogasiy.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Override
    public String getCommand() {
        return "about";
    }

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Display developer info";
    }

    @Override
    public void execute() {
        System.out.println("Developer: Alexey Pogasiy");
        System.out.println("e-mail: apogasiy@tsconsulting.com");
    }

}
