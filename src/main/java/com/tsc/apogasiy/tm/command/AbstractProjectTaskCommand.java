package main.java.com.tsc.apogasiy.tm.command;

import main.java.com.tsc.apogasiy.tm.exception.empty.EmptyNameException;
import main.java.com.tsc.apogasiy.tm.exception.entity.ProjectNotFoundException;
import main.java.com.tsc.apogasiy.tm.exception.entity.TaskNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.Project;
import main.java.com.tsc.apogasiy.tm.model.Task;

import java.util.List;

public abstract class AbstractProjectTaskCommand extends AbstractCommand{

    protected void showProjectTasks(Project project) {
        if (project == null)
            throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
        List<Task> tasks = serviceLocator.getProjectTaskService().findTaskByProjectId(project.getId());
        if (tasks.size() <= 0)
            throw new TaskNotFoundException();
        for (Task task : tasks) {
            System.out.println("Id: " + task.getId());
            System.out.println("Name: " + task.getName());
            System.out.println("Description: " + task.getDescription());
            System.out.println("Status: " + task.getStatus());
        }
    }

    protected Project add(final String name, final String description) {
        if (name == null || name.isEmpty())
            throw new EmptyNameException();
        return new Project(name, description);
    }

}
