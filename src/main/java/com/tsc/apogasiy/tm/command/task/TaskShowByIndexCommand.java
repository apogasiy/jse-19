package main.java.com.tsc.apogasiy.tm.command.task;

import main.java.com.tsc.apogasiy.tm.command.AbstractTaskCommand;
import main.java.com.tsc.apogasiy.tm.exception.entity.TaskNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.Task;
import main.java.com.tsc.apogasiy.tm.util.TerminalUtil;

public class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getCommand() {
        return "task-show-by-index";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show task by index";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber();
        final Task task = serviceLocator.getTaskService().findByIndex(index);
        if (task == null)
            throw new TaskNotFoundException();
        showTask(task);
    }

}
