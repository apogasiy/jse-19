package main.java.com.tsc.apogasiy.tm.command.system;

import main.java.com.tsc.apogasiy.tm.command.AbstractCommand;

import java.util.Collection;

public class ArgumentDisplayCommand extends AbstractCommand {

    @Override
    public String getCommand() {
        return null;
    }

    @Override
    public String getArgument() {
        return "-arg";
    }

    @Override
    public String getDescription() {
        return "Display list of arguments";
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> arguments = serviceLocator.getCommandService().getArguments();
        for (final AbstractCommand argument : arguments)
            System.out.println(argument.getCommand());
    }

}
