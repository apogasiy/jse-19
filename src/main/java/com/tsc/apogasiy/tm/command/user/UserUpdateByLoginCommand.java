package main.java.com.tsc.apogasiy.tm.command.user;

import main.java.com.tsc.apogasiy.tm.command.AbstractUserCommand;
import main.java.com.tsc.apogasiy.tm.exception.entity.UserNotFoundException;
import main.java.com.tsc.apogasiy.tm.exception.system.AccessDeniedException;
import main.java.com.tsc.apogasiy.tm.model.User;
import main.java.com.tsc.apogasiy.tm.util.TerminalUtil;

public class UserUpdateByLoginCommand extends AbstractUserCommand {

    @Override
    public String getCommand() {
        return "user-update-by-login";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Update user info by login";
    }

    @Override
    public void execute() {
        final boolean isAuth = serviceLocator.getAuthService().isAuth();
        if (!isAuth)
            throw new AccessDeniedException();
        System.out.println("Enter login:");
        final String login = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null)
            throw new UserNotFoundException();
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (currentUserId.equals(user.getId()))
            throw new AccessDeniedException();
        System.out.println("Enter last name:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter first name:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter middle name:");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("Enter email:");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateByLogin(login, lastName, firstName, middleName, email);
    }

}
