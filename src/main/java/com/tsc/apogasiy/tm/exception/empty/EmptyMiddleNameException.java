package main.java.com.tsc.apogasiy.tm.exception.empty;

import main.java.com.tsc.apogasiy.tm.exception.AbstractException;

public class EmptyMiddleNameException extends AbstractException {

    public EmptyMiddleNameException() {
        super("Error! Middle name is empty!");
    }

}
