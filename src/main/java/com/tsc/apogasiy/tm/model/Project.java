package main.java.com.tsc.apogasiy.tm.model;

import main.java.com.tsc.apogasiy.tm.api.entity.IWBS;
import main.java.com.tsc.apogasiy.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public class Project extends AbstractEntity implements IWBS {

    private String name;
    private String description;
    private Status status = Status.NOT_STARTED;
    private Date startDate = null;
    private Date finishDate = null;
    private Date created = new Date();

    public Project() {
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}
