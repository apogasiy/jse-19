package main.java.com.tsc.apogasiy.tm.repository;


import main.java.com.tsc.apogasiy.tm.api.repository.ICommandRepository;
import main.java.com.tsc.apogasiy.tm.command.AbstractCommand;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public AbstractCommand getCommandByName(String name) {
        return commands.get(name);
    }

    @Override
    public AbstractCommand getCommandByArg(String arg) {
        return arguments.get(arg);
    }

    @Override
    public Collection<String> getCommandNames() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : commands.values()) {
            final String name = command.getCommand();
            if (name == null || name.isEmpty())
                continue;
            result.add(name);
        }
        return result;
    }

    @Override
    public Collection<String> getArgNames() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : arguments.values()) {
            final String arg = command.getArgument();
            if (arg == null || arg.isEmpty())
                continue;
            result.add(arg);
        }
        return result;
    }

    @Override
    public void add(AbstractCommand command) {
        final String arg = command.getArgument();
        final String name = command.getCommand();
        if (arg != null)
            arguments.put(arg, command);
        if (name != null)
            commands.put(name, command);
    }

}
