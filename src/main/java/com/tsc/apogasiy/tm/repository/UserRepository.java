package main.java.com.tsc.apogasiy.tm.repository;

import main.java.com.tsc.apogasiy.tm.api.repository.IUserRepository;
import main.java.com.tsc.apogasiy.tm.exception.entity.UserNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(String login) {
        for (User user : list) {
            if (login.equals(user.getLogin()))
                return user;
        }
        return null;
    }

    @Override
    public User findByEmail(String email) {
        for (User user : list) {
            if (email.equals(user.getEmail()))
                return user;
        }
        throw new UserNotFoundException();
    }

    @Override
    public User removeByLogin(String login) {
        final User user = findByLogin(login);
        remove(user);
        return user;
    }

    @Override
    public boolean userExistsByLogin(String login) {
        return findByLogin(login) != null;
    }

    @Override
    public boolean userExistsByEmail(String email) {
        return findByEmail(email) != null;
    }

}
