package main.java.com.tsc.apogasiy.tm.repository;

import main.java.com.tsc.apogasiy.tm.api.repository.IRepository;
import main.java.com.tsc.apogasiy.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {
    protected final List<E> list = new ArrayList<>();

    public void add(final E entity) {
        list.add(entity);
    }

    public void remove(final E entity) {
        list.remove(entity);
    }

    public List<E> findAll() {
        return list;
    }

    public List<E> findAll(Comparator<E> comparator) {
        final List<E> entityList = new ArrayList<>(list);
        entityList.sort(comparator);
        return entityList;
    }

    public void clear() {
        list.clear();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public E findById(String id) {
        for (E entity : list)
            if (id.equals(entity.getId()))
                return entity;
        return null;
    }

    public E findByIndex(Integer index) {
        return list.get(index);
    }


    public boolean existsById(String id) {
        return findById(id) != null;
    }

    public boolean existsByIndex(Integer index) {
        return index < list.size() && index >= 0;
    }

    public E removeById(String id) {
        final E entity = findById(id);
        if (entity == null)
            return null;
        list.remove(entity);
        return entity;
    }
    public E removeByIndex(Integer index) {
        final E entity = findByIndex(index);
        if (entity == null)
            return null;
        list.remove(entity);
        return entity;
    }
}
