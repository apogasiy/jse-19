package main.java.com.tsc.apogasiy.tm.repository;

import main.java.com.tsc.apogasiy.tm.api.repository.ITaskRepository;
import main.java.com.tsc.apogasiy.tm.enumerated.Status;
import main.java.com.tsc.apogasiy.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllTaskByProjectId(String projectId) {
        List<Task> listByProject = new ArrayList<>();
        for (Task task : list)
            if (projectId.equals(task.getProjectId()))
                listByProject.add(task);
        return listByProject;
    }

    @Override
    public Task findByName(String name) {
        final Task entity = findByName(name);
        if (entity == null)
            return null;
        list.remove(entity);
        return entity;
    }

    @Override
    public Task removeByName(String name) {
        return null;
    }

    @Override
    public boolean existsByName(String name) {
        return findByName(name) != null;
    }

    @Override
    public Task startById(String id) {
        final Task task = findById(id);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(Integer index) {
        final Task task = findByIndex(index);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(String name) {
        final Task task = findByName(name);
        if (task == null)
            return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(String id) {
        final Task task = findById(id);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(Integer index) {
        final Task task = findByIndex(index);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(String name) {
        final Task task = findByName(name);
        if (task == null)
            return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeStatusById(String id, Status status) {
        final Task task = findById(id);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(Integer index, Status status) {
        final Task task = findByIndex(index);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(String name, Status status) {
        final Task task = findByName(name);
        if (task == null)
            return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task bindTaskToProjectById(final String projectId, final String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskById(final String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(final String projectId) {
        List<Task> listByProject = findAllTaskByProjectId(projectId);
        for (Task task : listByProject)
            list.remove(task);
    }
}
